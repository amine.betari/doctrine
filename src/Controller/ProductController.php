<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Product;
use App\Entity\Category;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Twig\Environment;

class ProductController extends AbstractController
{
    private $twig;

    public function __construct(Environment $twig)
    {
        $this->twig = $twig;
    }

    /**
     * @Route("/product", name="create_product")
     */
    public function index(ValidatorInterface $validator): Response
    {

        $category = new Category();
        $category->setName('Computer Peripherals');

 	    // you can fetch the EntityManager via $this->getDoctrine() // or you can add an argument to the action: createProduct(EntityManagerInterface $entityManager)
        $entityManager = $this->getDoctrine()->getManager();

        $product = new Product();
        $product->setName('Keyboard'. rand(1,99));
        $product->setPrice(1997);
        $product->setDescription('Ergonomic and stylish!');

        // relates this product to the category
        $product->setCategory($category);


        $errors = $validator->validate($product);
        if (count($errors) > 0) {
            return new Response((string) $errors, 400);
        }


        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($product);
        $entityManager->persist($category);
        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        return new Response('Saved new product with id '.$product->getId());
	
    }


    /**
     * @Route("/product/{id}", name="product_show")
     */
    public function show(Product $product): Response
//  public function show(int $id,  ProductRepository $productRepositor): Response
//  public function show(int $id): Response
    {

        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->find($product->getId());
        // Query de plus
        $category = $product->getCategory();
        dump($category);
        dump($category->getName());



       /* $product = $productRepository->find($id); */

        $product = $this->getDoctrine()
            ->getRepository(Product::class)
            ->findOneByIdJoinedToCategory($product->getId());
        // y a pas de query de plus
        $category = $product->getCategory();
        dump($category);
        dump($category->getName());


        return new Response($this->twig->render('products/show.html.twig', [
            'category' => $category->getName()
        ]));




        if (!$product) {
            throw $this->createNotFoundException(
                'No product found founded'
            );
        }

        $categoryName = $product->getCategory()->getName();

        return new Response('Check out this great product: '.$product->getName(). ' Category ==> '. $categoryName);

        // or render a template
        // in the template, print things with {{ product.name }}
        // return $this->render('product/show.html.twig', ['product' => $product]);
    }


    /**
     * @Route("/product/edit/{id}")
     */
    public function update(int $id): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $product = $entityManager->getRepository(Product::class)->find($id);

        if (!$product) {
            throw $this->createNotFoundException(
                'No product found for id '.$id
            );
        }

        $product->setName('New product name!'. rand(1,99));
        $entityManager->flush();

        return $this->redirectToRoute('product_show', [
            'id' => $product->getId()
        ]);
    }


    public function showProducts(int $id)
    {
        $category = $this->getDoctrine()
            ->getRepository(Category::class)
            ->find($id);

        $products = $category->getProducts();

        // ...
    }
}
